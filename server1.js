const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const axios = require('axios');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);

//const apiUrl = 'http://3.130.150.63:8080/api/position/last/2/1/';
//const bearerToken = '';

// Mapa para almacenar conexiones asociadas a idDispositivo y el identificador del intervalo
const dispositivoSockets = new Map();

// Función para obtener datos de la API REST y notificar a los clientes conectados
function fetchDataAndNotifyClients(idDispositivo, socket, token) {
  // Configura la URL de la API REST con el idDispositivo proporcionado
  const apiUrlWithId = `http://3.130.150.63:8080/api/position/last/2/${idDispositivo}/`;

  // Configura la solicitud HTTP con el token en los encabezados
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  // Realiza la solicitud HTTP a tu API REST
  axios.get(apiUrlWithId, config)
    .then(response => {
      // Verifica si el código de estado es 200 (OK)
      if (response.status === 200) {
        // Obtiene los nuevos datos de la respuesta de la API
        const newData = response.data;

        // Emite la información al socket asociado al idDispositivo
        socket.emit('dataUpdate', newData);
        // Agrega un console.log para imprimir un mensaje cuando hay actualizaciones
        console.log(`Datos actualizados para idDispositivo ${idDispositivo}:`, newData);
      } else {
        console.error(`Error al obtener datos de la API. Código de estado: ${response.status}`);
      }
    })
    .catch(error => {
      console.error('Error al obtener datos de la API:', error.message);
    });
}

// Configura Socket.IO para manejar conexiones de clientes
io.on('connection', (socket) => {
  console.log('Cliente conectado');

  // Maneja la conexión del cliente
  socket.on('disconnect', () => {
    console.log('Cliente desconectado');

    // Obtén el idDispositivo asociado a este socket
    let idDispositivo = null;
    dispositivoSockets.forEach((value, key) => {
      if (value.socket === socket) {
        idDispositivo = key;
      }
    });

    // Detén el intervalo si existe
    if (idDispositivo && dispositivoSockets.has(idDispositivo)) {
      const { intervalId } = dispositivoSockets.get(idDispositivo);
      clearInterval(intervalId);
    }

    // Elimina la entrada del mapa cuando un cliente se desconecta
    dispositivoSockets.delete(idDispositivo);
  });

  // Maneja el evento 'join' enviado por el cliente
  socket.on('join', (data) => {
    const idDispositivo = data.idDispositivo;
    const token = data.token; // Obtén el token desde los datos enviados por el cliente
    console.log(`Cliente con idDispositivo ${idDispositivo} se ha unido.`);

    // Llama a la función para obtener datos y notificar a los clientes
    fetchDataAndNotifyClients(idDispositivo, socket, token);

    // Almacena el identificador del intervalo para detenerlo más tarde
    const intervalId = setInterval(() => {
      fetchDataAndNotifyClients(idDispositivo, socket, token);
    }, 30000);

    // Almacena el socket y el identificador del intervalo en el mapa
    dispositivoSockets.set(idDispositivo, { socket, intervalId });
  });
});

// Inicia el servidor
const PORT = process.env.PORT || 4173;
server.listen(PORT, () => {
  console.log(`Servidor en ejecución en http://localhost:${PORT}`);
});
